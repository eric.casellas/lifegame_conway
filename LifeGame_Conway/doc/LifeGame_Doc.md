---
title: "LifeGame_Conway package documentation"
---


# Package LifeGame_Conway for vle-2.0.2

## Simulator general info

VLE package name : **LifeGame_Conway** 

## General Description

[Wikipedia link](https://en.wikipedia.org/wiki/Conway's_Game_of_Life)   
The Game of Life, is a cellular automaton devised by the British mathematician John Horton Conway (26 December 1937 – 11 April 2020) in 1970

The universe of the Game of Life is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead, or "populated" or "unpopulated". Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:

1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

The initial pattern constitutes the seed of the system. The first generation is created by applying the above rules simultaneously to every cell in the seed—births and deaths occur simultaneously, and the discrete moment at which this happens is sometimes called a tick (in other words, each generation is a pure function of the preceding one). The rules continue to be applied repeatedly to create further generations.

## Examples of patterns

### Oscillators

![blinker](figures/LifeGame_blinker.gif)
![taod](figures/LifeGame_toad.gif)
![beacon](figures/LifeGame_beacon.gif)

### Spaceships

![glider](figures/LifeGame_glider.gif)

### Infinite growth

![gosper glider gun](figures/LifeGame_gosperglidergun.gif)



## LifeGame.vpz simulator desciption

The LifeGame.vpz simulator use the Builder model from vle.generic.builder system package, initializing the world grid and coupling all cells with each other using experimental conditions.
Each cell is an instance of the CellModel Discrete-time model


## Parameters of the CellModel

dbglog 
:   (double) verbose control  

initial_state 
:   (int) explicit initialisation of a single cell (optionnal)  

init_map 
:   (matrix) explicit initialisation of all cells, use model name to get position (optionnal, if initial_state not provided)

init_proba 
:   (double) use random generator with given probability (optionnal, if neither initial_state nor init_map are provided)  

split_outputs 
:   (bool) are output ports splitted for each neighbour(default is true, if set to false a single CellState output is handled)  
