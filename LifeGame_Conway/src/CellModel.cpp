/**
  * @file CellModel.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <vle/vpz/BaseModel.hpp>
#include <vle/value/Matrix.hpp>

#include <iostream>
#include <random>

namespace vd = vle::devs;
namespace vz = vle::vpz;
namespace vv = vle::value;

namespace LifeGame_Conway {

using namespace vle::discrete_time;

class CellModel : public DiscreteTimeDyn
{
public:
CellModel(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts), firstcompute(true), init_proba(0.)
{
        dbglog = evts.exist("dbglog") ? vv::toDouble(evts.get("dbglog")) : 0.;
        if (dbglog>0) std::cout << "Starting " << getModel().getCompleteName() << std::endl;
        
        CellState.init(this, "CellState", evts);
        int init_state;
        if (!evts.exist("initial_state")) {
            if (evts.exist("init_map")) { // user defined initial matrix (using model name to get its position)
                const vv::Matrix& init_map = evts.getMatrix("init_map");
                // guess position from model name. expected name sample "top,m-0-0,cell"
                int colnum,rownum;
                std::string s = getModel().getCompleteName();
                std::string delimiter = "-";
                size_t pos1, pos2, pos3;
                pos1 = s.find(delimiter); // position of first - delimiter
                pos2 = s.substr(pos1+1).find(delimiter); // position - delimiter (after pos1)
                delimiter = ",";
                pos3 = s.substr(pos1+1+pos2+1).find(delimiter); // position of , delimiter (after pos2)
                colnum = std::stoi(s.substr(pos1+1, pos2)); 
                rownum = std::stoi(s.substr(pos1+1+pos2+1, pos3)); 
                init_state = (int)init_map.getBoolean(colnum,rownum);
            } else { // random generator
                init_proba = evts.exist("init_proba") ? vv::toDouble(evts.get("init_proba")) : 0.4;
                std::random_device rdev{};
                std::default_random_engine generator{rdev()};
                std::bernoulli_distribution distribution(init_proba);
                init_state = (int)distribution(generator);
            }
        } else { // user defined initial value
            init_state = vv::toInteger(evts.get("initial_state"));
        }
        CellState.init_value(init_state);
        if (dbglog>0) std::cout << "\t\tInitial state : " << init_state << std::endl;

        split_outputs = evts.exist("split_outputs") ? vv::toBoolean(evts.get("split_outputs")) : true;

        std::string vv[8] = {"E", "N", "NE", "NW", "S", "SE", "SW", "W"};
        portnames.assign(&vv[0], &vv[0]+8);
        for (std::vector< std::string >::iterator it = portnames.begin(); it != portnames.end(); ++it) {
            Var* v = new Var();
            v->init(this, "in_"+(*it), evts);
            inputs.push_back(*v);
            if (split_outputs) {
                Var* v2 = new Var();
                v2->init(this, "out_"+(*it), evts);
                v2->init_value(init_state);
                outputs.push_back(*v2);
            }
        }
}

virtual ~CellModel()
{}


void compute(const vle::devs::Time& t)
{
    if (dbglog>1) std::cout << t << "\tCompute " << getModel().getCompleteName() << std::endl;

    if (firstcompute) { // ignore first compute to give time for all cells to be interconnected for sending&receiving the initial value for/from neighbour cells
        CellState = CellState(-1);
        firstcompute = false;
    } else {

        int sum = 0.; // counting the number of living neighbours
        for (std::vector< Var >::iterator it = inputs.begin(); it != inputs.end(); ++it) {
            sum = sum + (int)((*it)(-1));
        }
        
        if (CellState()>0) { //for living cell
            if (sum<2) { // die if less than 2 living neighbours (underpopulation)
                CellState = 0.;
            } else if (sum<=3) { // stay alive if 2 or 3 living neighbours
                CellState = 1.;
            } else { //die if more than 3 living neighbours (overpopulation)
                CellState = 0.;
            }
        } else { // for dead cell
            if (sum==3) {
                CellState = 1.; // new born (reproduction)
            } else {
                CellState = 0.; // remain dead
            }
        }
        
        if (dbglog>1) std::cout << "\t\tsum: " << sum  << "\tState: " << CellState(-1)<< "\t->\t" << CellState() << std::endl;
    }

    if (split_outputs) {
        for (std::vector< Var >::iterator it = outputs.begin(); it != outputs.end(); ++it) { // update all output ports 
            (*it) = CellState();
        }
    }
}


private:
    Var CellState;

    std::vector< std::string > portnames;
    std::vector< Var > inputs;
    std::vector< Var > outputs;

    bool firstcompute;
    double dbglog;
    double init_proba;
    bool split_outputs;

};

} // namespace LifeGame_Conway


DECLARE_DYNAMICS(LifeGame_Conway::CellModel)

