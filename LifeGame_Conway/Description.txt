Package: LifeGame_Conway
Version: 0.1.0
Depends: vle (=2.0)
Build-Depends: vle.discrete-time, vle.generic.builder
Conflicts:
Maintainer: Eric Casellas  <Eric.Casellas@inrae.fr>
Description: Life game (Conway) using vle::discrete_time cells and vle.generic.builder Builder model to generate cells and their connexions
 .
Tags: examples
Url: https://www.vle-project.org/2.0
Size: 0
MD5sum: xxxx
